package com.example.myapplication.view.event;

public interface OnCallBackToView {
    void showLoading();

    void hideLoading();
}
