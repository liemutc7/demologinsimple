package com.example.myapplication.view.activity;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseActivity;
import com.example.myapplication.presenter.MainPresenter;
import com.example.myapplication.view.event.OnMainCallBack;
import com.example.myapplication.view.fragment.FragmentLogin;

public class MainActivity extends BaseActivity<MainPresenter> implements OnMainCallBack {


    @Override
    protected MainPresenter getPresenter() {
        return new MainPresenter(this);
    }

    @Override
    protected void initView() {
        showFragment(FragmentLogin.TAG);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected int getContentId() {
        return R.id.frame_layout;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
