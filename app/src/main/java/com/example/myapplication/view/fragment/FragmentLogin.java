package com.example.myapplication.view.fragment;

import android.view.View;
import android.widget.EditText;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseFragment;
import com.example.myapplication.presenter.LoginPresenter;
import com.example.myapplication.view.event.OnLoginCallBack;

public class FragmentLogin extends BaseFragment<LoginPresenter> implements OnLoginCallBack, View.OnClickListener {

    public static final String TAG = FragmentLogin.class.getName();

    private EditText edtUser,edtPass;

    @Override
    protected LoginPresenter getPresenter() {
        return new LoginPresenter(this);
    }

    @Override
    protected void initView() {
        findViewById(R.id.btn_login,this);
        edtUser = findViewById(R.id.edt_user);
        edtPass = findViewById(R.id.edt_pass);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_login:
                if(edtUser.getText().toString().isEmpty() || edtPass.getText().toString().isEmpty()){
                    showNotify("Vui lòng nhập user hoặc pass còn thiếu");
                }else {
                    mPresenter.checkUserLogin(edtUser.getText().toString(),edtPass.getText().toString());
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void toLoginSuccess() {
        getParent().showFragment(FragmentHome.TAG);
    }

    @Override
    public void toLoginFail() {
        showNotify("Bạn đã nhập sai user hoặc pass");
    }
}
