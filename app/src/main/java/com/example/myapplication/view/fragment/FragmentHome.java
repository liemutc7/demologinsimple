package com.example.myapplication.view.fragment;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseFragment;
import com.example.myapplication.presenter.HomePresenter;
import com.example.myapplication.view.event.OnHomeCallBack;

public class FragmentHome extends BaseFragment<HomePresenter> implements OnHomeCallBack {

    public static final String TAG = FragmentHome.class.getName();

    @Override
    protected HomePresenter getPresenter() {
        return new HomePresenter(this);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
