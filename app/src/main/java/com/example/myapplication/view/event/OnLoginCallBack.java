package com.example.myapplication.view.event;

public interface OnLoginCallBack extends OnCallBackToView{
    void toLoginSuccess();

    void toLoginFail();
}
