package com.example.myapplication.base;


import com.example.myapplication.view.event.OnCallBackToView;

public abstract class BasePresenter<T extends OnCallBackToView> {
    protected T mCallBack;

    public BasePresenter(T mCallBack) {
        this.mCallBack = mCallBack;
    }

}
