package com.example.myapplication.presenter;

import com.example.myapplication.base.BasePresenter;
import com.example.myapplication.view.event.OnLoginCallBack;

public class LoginPresenter extends BasePresenter<OnLoginCallBack> {
    public LoginPresenter(OnLoginCallBack mCallBack) {
        super(mCallBack);
    }

    public void checkUserLogin(String user, String pass) {
        if(user.equals("abc")&&pass.equals("123")){
            mCallBack.toLoginSuccess();
        }else {
            mCallBack.toLoginFail();
        }
    }
}
