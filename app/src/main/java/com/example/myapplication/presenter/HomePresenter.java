package com.example.myapplication.presenter;

import com.example.myapplication.base.BasePresenter;
import com.example.myapplication.view.event.OnHomeCallBack;

public class HomePresenter extends BasePresenter<OnHomeCallBack> {
    public HomePresenter(OnHomeCallBack mCallBack) {
        super(mCallBack);
    }
}
