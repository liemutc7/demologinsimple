package com.example.myapplication.presenter;

import com.example.myapplication.base.BasePresenter;
import com.example.myapplication.view.event.OnMainCallBack;

public class MainPresenter extends BasePresenter<OnMainCallBack> {
    public MainPresenter(OnMainCallBack mCallBack) {
        super(mCallBack);
    }
}
